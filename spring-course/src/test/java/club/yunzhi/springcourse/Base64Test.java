package club.yunzhi.springcourse;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * Bas64l转码测试
 */
public class Base64Test {

  @Test
  public void test() {
    String usernameAndPassword = "zhangsan:yunzhi.club";
    String result = Base64.getEncoder().encodeToString(usernameAndPassword.getBytes(StandardCharsets.UTF_8));
    System.out.println(result);
  }

  @Test
  public void decode()  {
    String input = "emhhbmdzYW46eXVuemhpLmNsdWI=";
    byte[] result = Base64.getDecoder().decode(input);
    System.out.println(new String(result));

  }
}
