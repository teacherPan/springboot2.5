package club.yunzhi.springcourse.repository;

import club.yunzhi.springcourse.entity.Teacher;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * 教师
 */
public interface TeacherRepository extends CrudRepository<Teacher, Long> {
  /**
   * 通过username来查找教师
   *
   * @param username 用户名
   * @return
   */
  Optional<Teacher> findByUsername(String username);
}
